package cv10.tests;

import cv10.base.TestBaseSetup;
import cv10.pages.pagemenus.HeaderPanel;
import cv10.pages.pagemenus.LeftSideBarMenu;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import cv10.pages.*;


public class WelcomePageTest extends TestBaseSetup {
    private WebDriver driver;
    private LogInPage logInPage;
    private WelcomePage welcomePage;
    private HeaderPanel headerPanel;
    private LeftSideBarMenu leftSideBar;
    private DBSourcePage dbSourcePage;

    @BeforeClass
    public void setUp() {
        driver = getDriver();
    }

    @Test
    public void verifyProjectLookUp() {
        System.out.println("Look up project test started...");
        logInPage = new LogInPage(driver);
        welcomePage = logInPage.userLogin();
        welcomePage.allBranchesLinkClick();
        welcomePage.projectLookUpButtonClick();
        welcomePage.enterProjectName("StatCan_MS_UKR_QA");
        Assert.assertTrue(welcomePage.verifyProjectFound(), "The project wasn't found");
    }

    @Test(dependsOnMethods = {"verifyProjectLookUp"})
    public void verifyHeaderUserLogOut() {
        System.out.println("Log out test started...");
        headerPanel = new HeaderPanel(driver);
        headerPanel.clickOnUserButton();
        headerPanel.clickOnLogOutButton();
        logInPage = headerPanel.clickOnLogOutButton();
        try {
            WebElement dynamicElement = (new WebDriverWait(driver, 10))
                    .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@class='login-submit']"))); //?
            Assert.assertTrue(logInPage.verifyLogInPageOpen("Log in"),"User logout doesn't work");
        } catch (TimeoutException ex){
            Assert.fail();
        }
    }

    @Test(dependsOnMethods = {"verifyProjectLookUp","verifyHeaderUserLogOut"})
    public void verifyDBSourcePageOpen() {
        logInPage = new LogInPage(driver);
        welcomePage = logInPage.userLogin();
        leftSideBar = new LeftSideBarMenu(driver);
        leftSideBar.clickOnMenuButton();
        leftSideBar.clickOnSystemSettingsButton();
        dbSourcePage = leftSideBar.clickOnDBSourceButton();
        try {
            WebElement dynamicElement = (new WebDriverWait(driver, 30))
                    .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[text()='DB Sources']"))); //?
            Assert.assertTrue(logInPage.verifyLogInPageOpen("System Settings"), "User logout doesn't work");
        } catch (TimeoutException ex){
                Assert.fail();
            }
        }
}
