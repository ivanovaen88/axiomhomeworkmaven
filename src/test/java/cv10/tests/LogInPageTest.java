package cv10.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import cv10.base.TestBaseSetup;
import org.testng.annotations.Test;
import cv10.pages.*;

public class LogInPageTest extends TestBaseSetup {
    private WebDriver driver;

    @BeforeClass
    public void setUp(){
        System.out.println("setUp");
        driver=getDriver();
    }

    @Test
    public void verifyLogInPageTitle(){
        System.out.println("LogIn loading test started...");
        LogInPage logInPage = new LogInPage(driver);
        Assert.assertTrue(logInPage.verifyLogInPageOpen("Log in"),"Log In page title doesn't match");
    }
}
