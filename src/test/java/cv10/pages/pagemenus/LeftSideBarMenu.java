package cv10.pages.pagemenus;

import cv10.base.TestBaseSetup;
import cv10.pages.DBSourcePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LeftSideBarMenu extends TestBaseSetup {
    private WebDriver driver;

    private By menuButton = By.xpath("//*[@id='gwt-uid-3']");
    private By collapseButton = By.xpath("(//*[@id='ic-collapsed']/span[@class='v-button-wrap'])[3]");
    private By dbSourceButton = By.xpath("//*[@id='DB_Source']/span/div/span");

    public LeftSideBarMenu(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnMenuButton() {
        WebElement element = driver.findElement(menuButton);
        element.click();
    }

    public void clickOnSystemSettingsButton() {
        WebElement element = driver.findElement(collapseButton);
        element.click();
    }

    public DBSourcePage clickOnDBSourceButton() {
        WebElement element = driver.findElement(dbSourceButton);
        element.click();
        return new DBSourcePage(driver);
    }
}
