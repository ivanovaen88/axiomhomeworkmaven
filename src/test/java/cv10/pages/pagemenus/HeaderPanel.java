package cv10.pages.pagemenus;

import cv10.base.TestBaseSetup;
import cv10.pages.LogInPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HeaderPanel extends TestBaseSetup {
    private WebDriver driver;

    private By userButton = By.xpath("//*[@id='header-user-icon']/*/*");
    private By logOutButton = By.xpath("//*[@id='ic-logout-last_Log_out']");

    public HeaderPanel(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnUserButton(){
        WebElement element = driver.findElement(userButton);
        element.click();
    }

    public LogInPage clickOnLogOutButton() {
        WebElement element = driver.findElement(logOutButton);
        element.click();
        return new LogInPage(driver);
    }

}
