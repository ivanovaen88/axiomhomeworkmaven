package cv10.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogInPage {
    private WebDriver driver;
    private By userNameTextBox = By.id("username");
    private By passwordTextBox = By.id("password");
    private By logInButton = By.xpath("//input[@class='login-submit']");

    public LogInPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean verifyLogInPageOpen(String expectedTitle){
        return driver.getTitle().contains(expectedTitle);
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public void enterUserName(String userName){
        WebElement element = driver.findElement(userNameTextBox);
        element.sendKeys(userName);
    }

    public void enterPassword(String password){
        WebElement element = driver.findElement(passwordTextBox);
        element.sendKeys(password);
    }

    public WelcomePage userLogin(){
        enterUserName("xxxxxxxx");
        enterPassword("xxxxxxx");
        WebElement element = driver.findElement(logInButton);
        element.click();
        return new WelcomePage(driver);
    }

}
