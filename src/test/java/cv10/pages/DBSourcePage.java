package cv10.pages;

import org.openqa.selenium.WebDriver;

public class DBSourcePage {
    private WebDriver driver;


    public DBSourcePage(WebDriver driver) {
        this.driver = driver;
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public boolean verifyPageTitle(String expectedTitle){
        return getTitle().contains(expectedTitle);
    }
}
