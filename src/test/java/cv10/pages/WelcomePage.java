package cv10.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WelcomePage {

    private String projectName;

    private WebDriver driver;
    private By allProjectsLink = By.xpath("//div[@id='ALL_BRANCHES']//span[text()='ALL BRANCHES']");
    private By projectLookUpButton = By.xpath("//*[contains(@class,'split-pane-component-1')]//*[@id='ic-lookup-big']");
    private By projectLookUpTextBox = By.xpath("//div[@class='v-input-prompt-wrapper v-widget has-caption v-input-prompt-wrapper-has-caption cv-search-field-input-text-field v-input-prompt-wrapper-cv-search-field-input-text-field v-has-width']//input");
    //private By rightSideMenuButton = By.xpath("//*[@id='gwt-uid-3']");

    private By getFoundProjectFieldLocator() {
        return By.xpath("//div[@class='v-tree v-scrollable v-widget cv-project-branch-tree v-tree-cv-project-branch-tree v-has-width v-has-height']//*[text()='" + projectName + "']");
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public WelcomePage(WebDriver driver) {
        this.driver = driver;
    }

    public String getTitle(){
        return driver.getTitle();
    }


    public void allBranchesLinkClick(){
        WebElement element = driver.findElement(allProjectsLink);
        element.click();
    }

    public void projectLookUpButtonClick(){
        WebElement element = driver.findElement(projectLookUpButton);
        element.click();
    }

    public void enterProjectName(String projectName) {
        WebElement element = driver.findElement(projectLookUpTextBox);
        element.click();
        element.sendKeys(projectName);
        setProjectName(projectName);

    }

    public boolean verifyProjectFound(){
        return !driver.findElements(getFoundProjectFieldLocator()).isEmpty();
//
//    boolean present;
//        try {
//            driver.findElement(By.id("logoutLink"));
//            present = true;
//        } catch (NoSuchElementException e) {
//            present = false;
//        }
    }

}
