package cv10.base;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

public class TestBaseSetup {
    private WebDriver driver;
//    static String driverPath = "D:\\\\drivers\\";
    static String driverPath = "src\\main\\resources\\drivers\\";

    public WebDriver getDriver() {
//        System.out.println("getDriver");
        return driver;
    }

//    public TestBaseSetup(){
//        System.out.println("Constructor Test Base setup");
//    }

    public void setDriver(String browserType, String appURL) {
        if ("chrome".equals(browserType)) {
            driver = initChromeDriver(appURL);
        } else if ("ie".equals(browserType)) {
            driver = initIE11Driver(appURL);
        } else {
            System.out.println("browser: " + browserType + " is invalid for choice..");
            driver = initChromeDriver(appURL);
        }
        System.out.println("setDriver");
    }
    private static WebDriver initChromeDriver(String appURL) {
        System.out.println("Launching google chrome with new profile...");
        System.setProperty("webdriver.chrome.driver",driverPath + "chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--ignore-certificate-errors");
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        WebDriver driver = new ChromeDriver(cap);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        driver.navigate().to(appURL);
        return driver;
    }

    private static WebDriver initIE11Driver(String appURL){
        System.out.println("Launching IE11 with new profile...");
        DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
        ieCapabilities.setCapability(
                InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
                true
        );
        System.setProperty("webdriver.ie.driver",driverPath + "IEDriverServer.exe");
        WebDriver driver = new InternetExplorerDriver(ieCapabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.navigate().to(appURL);
        return driver;
    }

    @Parameters({"browserType","appURL"})
    @BeforeClass
    public void initializeTestBaseSetup(String browserType, String appURL){
//        System.out.println("initializeTestBaseSetup");
        try{
            setDriver(browserType, appURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public void tearDown(){
//        System.out.println("tearDown");
//        driver.quit();
    }
}
